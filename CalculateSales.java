package jp.alhinc.suzuki_keita.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;




public class CalculateSales {
	public static void main(String[] args) {

		// 支店ファイルの入力
		BufferedReader brBranch = null;
		HashMap<String, String> mapBranch = new HashMap<String, String>(); //支店mapの生成
		HashMap<String,Long> mapCulculate = new HashMap<String,Long>(); //売り上げ集計mapの作成

		try {
		File fileBranch = new File(args[0], "branch.lst");
		FileReader frBranch = new FileReader(fileBranch);
		brBranch = new BufferedReader(frBranch);

		String lineBranch;

			while((lineBranch = brBranch.readLine()) !=null) {

				if(! (lineBranch.matches("[0-9]{3},.+"))) {
					System.out.println("支店定義ファイルのフォーマットが違います。");
					return;
				}

				String[] branchSplit = lineBranch.split(",", 0);  //コードと支店名に分割
				mapBranch.put(branchSplit[0],branchSplit[1]);
				mapCulculate.put(branchSplit[0], 0L );

			}

		}

		catch(FileNotFoundException fnfe) {
			System.out.println("支店定義ファイルが存在しません。");

		}

		catch(IOException e){
			System.out.println("予期せぬエラーが発生しました。");
		}

		finally {
		if(brBranch != null){
			try {
				brBranch.close();
			}
			catch(IOException e) {
				System.out.println("支店定義ファイルをクローズできませんでした。");
				}
				}
			}

		System.out.println(mapBranch.entrySet()); //mapBanchの参照
		System.out.println(mapCulculate.entrySet()); //mapBanchの参照



	//売り上げデータの入力
	BufferedReader brSales = null;
	ArrayList<File> SalesSelection = new ArrayList<File>(); //選別用リスト

	File fileSales = new File(args[0]);
	File[] listSales = fileSales.listFiles();


	//ファイルの選別
	for(File SalesFor : listSales) {
		if((SalesFor.getName().matches(".*[0-9]{8}.*")) && (SalesFor.getName().matches(".*rcd.*"))) {
			SalesSelection.add(SalesFor);

		}
	}

	System.out.println(SalesSelection); //売り上げファイルを選別できているかの確認


	//SalesArrayへの格納
	for(int i = 0; i < SalesSelection.size(); i++) {
		ArrayList<String> CulculateList = new ArrayList<String>(); //[0][1]のみを使うためにここに生成

			try {
				FileReader frSales = new FileReader(SalesSelection.get(i));
				brSales = new BufferedReader(frSales);
				String lineSales;

				while((lineSales = brSales.readLine()) != null) {
					//売り上げファイルを読み込み、集計リストに格納（0,1のみ）
				CulculateList.add(lineSales);
				}

				//支店コードのエラーチェック
				if(! CulculateList.get(0).matches("mapCulculate.ketSet()")) {


				}
				//keyが同じものに対して、売り上げを足していく
				Long  Summary = mapCulculate.get(CulculateList.get(0)) ;
				Long j = Long.parseLong(CulculateList.get(1));
				Summary += j ;
				mapCulculate.put(CulculateList.get(0),Summary );

				//10桁チェック
				String SummaryCheck = String.valueOf(Summary);
					if(SummaryCheck.matches(".*[0-9]{10,}.*")) {
					System.out.println("合計金額が10桁を超えました");
					return;
					}

			}

				catch(IOException e){
					System.out.println("売り上げファイルでエラーが発生しました。");
				}
				finally {
					if(brSales != null) {
						try {
							brSales.close();
						}
							catch(IOException e) {
								System.out.println("売り上げファイルをクローズできませんでした。");
							}
					}
				}
	}
	System.out.println(mapCulculate.entrySet()); //mapBanchの参照

	//ファイルへの出力
	BufferedWriter bwOut = null;
	try {
		File OutFile = new File(args[0],"branch.out");
		FileWriter fwOut = new FileWriter(OutFile);
		bwOut = new BufferedWriter(fwOut);

		for(String keyOut : mapBranch.keySet()) {
			bwOut.write(keyOut + "," + mapBranch.get(keyOut) + "," + mapCulculate.get(keyOut) + "\r\n");

		}
	}
	catch(IOException e) {
		System.out.println("ファイル出力でエラーが発生しました。");
	}
	finally{
		try {
			bwOut.close();
		}
		catch(IOException e) {
			System.out.println("出力ファイルをクローズできませんでした。");
		}
	}








}


}
