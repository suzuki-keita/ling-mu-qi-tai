package jp.alhinc.suzuki_keita.calculate_sales;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;




public class CalculateSales {
	
	int code;		//支店コード
	String name;	//支店名
	long cost;		//集計金額
	
	CalculateSales(int code, String name){
		this.code = code;
		this.name = name;
	}
	
	void check() {
		System.out.println(this.code);
		System.out.println(this.name);
	}
	
	public static void main(String[] args) {
		
		// 支店ファイルの入力
		BufferedReader brBranch = null;
	try {
	File fileBranch = new File(args[0], "branch.lst");
	FileReader frBranch = new FileReader(fileBranch);
	brBranch = new BufferedReader(frBranch);
	
	String lineBranch;
	while((lineBranch = brBranch.readLine()) !=null) {
		String[] branchSplit = lineBranch.split(",", 0);  //コードと支店名に分割
		
		HashMap<String, String> map = new HashMap<String, String>(); //mapへの格納 
		map.put(branchSplit[0],branchSplit[1]);	 
		
		System.out.println(map.entrySet());

		
	}	
		}
	catch(IOException e){
	System.out.println("支店定義ファイルでエラーが発生しました。");
	}
	finally {
	if(brBranch != null){
		try {
			brBranch.close();
		}
		catch(IOException e) {
			System.out.println("支店定義ファイルをクローズできませんでした。"); 
			}
			}
		}
	
	//売り上げデータの入力	
	File fileSales = new File(args[0]);
	File[] listSales = fileSales.listFiles();
	

	for(int i =0; i < listSales.length; i++) {
		if(listSales[i].getName().matches(".*[0-9]{8}.*")) {
			
			String[] SalesFiles = new String[listSales.length];
			BufferedReader brSales = null;
			try {
			FileReader frSales = new FileReader(SalesFiles[i]);
			brSales = new BufferedReader(frSales);
			
			String lineSales;
			while((lineSales = brSales.readLine()) != null) {
				String[] SalesSplit = lineSales.split("\n");
				
				HashMap<String,String> map = new HashMap<String,String>();
				map.put(SalesSplit[0],SalesSplit[1]);
				
				System.out.println(map.entrySet());
			}
			
			
			
			}catch(IOException e) {
				System.out.println("売り上げファイル取得でエラーが出ました");
			}
			finally {
				if(brSales != null) {
				try{
					brSales.close();
				}catch(IOException e) {
					System.out.println("売り上げファイルをクローズできませんでした");
				}
			}
			
		
		}
	
	
		
	}
}
}}
	


